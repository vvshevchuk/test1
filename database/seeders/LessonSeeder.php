<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++)
            DB::table('lessons')->insert([
                'category_id' => rand(1, 4),
                'title' => "Lesson {$i}",
                'slug' => "lesson-{$i}",
                'description' => "description of lesson {$i}",
                'text' => "<p>It is a long established fact that a reader will be distracted by
                 the readable content of a page when looking at its layout. The point of using
                 Lorem Ipsum is that it has a more-or-less normal distribution of letters, as
                 opposed to using 'Content here, content here', making it look like readable English.
                 Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                 default model text, and a search for 'lorem ipsum' will uncover many web sites still
                  in their infancy. Various versions have evolved over the years, sometimes by accident,
                   sometimes on purpose (injected humour and the like). {$i}</p>",
            ]);
    }
}
