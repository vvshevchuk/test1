<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Lesson;

class LessonController extends Controller
{
    public function getLessonsByCategory(string $slug)
    {
        $categories = Category::orderBy('title')->get();
        $currentCategory = Category::where('slug', $slug)->first();
        return view('main', [
            'lessons' => $currentCategory->lessons()->paginate(4),
            'categories' => $categories,
        ]);
    }

    public function getLesson(string $slug)
    {
        $lesson = Lesson::where('slug', $slug)->first();
        return view('show-lesson', [
            'lesson' => $lesson,
        ]);
    }
}
